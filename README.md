# THE MILKMAN

A game made in Unreal Engine 5.0.3 in under 48 hours for Ludum Dare 51.

YOu can find a playable build [here](https://ldjam.com/events/ludum-dare/51/milkman)

Source requires ALS Community v4 that can be found [here](https://github.com/dyanikoglu/ALS-Community/)