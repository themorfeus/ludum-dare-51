// Copyright Epic Games, Inc. All Rights Reserved.

using UnrealBuildTool;

public class LD51 : ModuleRules
{
	public LD51(ReadOnlyTargetRules Target) : base(Target)
	{
		PCHUsage = PCHUsageMode.UseExplicitOrSharedPCHs;

		PrivateDependencyModuleNames.AddRange(new string[]
		{
			"ALSV4_CPP", 
			"EnhancedInput",
			"Core", 
			"CoreUObject", 
			"Engine", 
			"InputCore", 
			"HeadMountedDisplay"
		});
	}
}
