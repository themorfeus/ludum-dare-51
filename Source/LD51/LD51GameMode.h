// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "LD51GameMode.generated.h"

UCLASS(minimalapi)
class ALD51GameMode : public AGameModeBase
{
	GENERATED_BODY()

public:
	ALD51GameMode();
};



