// Copyright Epic Games, Inc. All Rights Reserved.

#include "LD51GameMode.h"
#include "LD51Character.h"
#include "UObject/ConstructorHelpers.h"

ALD51GameMode::ALD51GameMode()
{
	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnBPClass(TEXT("/Game/ThirdPerson/Blueprints/BP_ThirdPersonCharacter"));
	if (PlayerPawnBPClass.Class != NULL)
	{
		DefaultPawnClass = PlayerPawnBPClass.Class;
	}
}
