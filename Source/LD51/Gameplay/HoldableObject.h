﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "HoldableObject.generated.h"

UCLASS()
class LD51_API AHoldableObject : public AActor
{
	GENERATED_BODY()

public:
	AHoldableObject();

	UFUNCTION(BlueprintCallable, BlueprintImplementableEvent)
	void OnPickedUp(USceneComponent* TargetComponent);
	
	UFUNCTION(BlueprintCallable, BlueprintImplementableEvent)
	void OnDropped();
	
	UFUNCTION(BlueprintCallable, BlueprintImplementableEvent)
	void OnTargeted();
	
	UFUNCTION(BlueprintCallable, BlueprintImplementableEvent)
	void OnUntargeted();

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly)
	bool AutoAttach = true;
};
