﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Character/ALSBaseCharacter.h"
#include "UObject/Object.h"
#include "LCharacterBase.generated.h"

class AHoldableObject;
/**
 * 
 */
UCLASS()
class LD51_API ALCharacterBase : public AALSBaseCharacter
{
	GENERATED_BODY()
	
public:
	ALCharacterBase(const FObjectInitializer& ObjectInitializer);
	
	/** Implemented on BP to update held objects */
	UFUNCTION(BlueprintImplementableEvent, BlueprintCallable, Category = "ALS|HeldObject")
	void UpdateHeldObject();

	UFUNCTION(BlueprintCallable, Category = "ALS|HeldObject")
	void ClearHeldObject();

	UFUNCTION(BlueprintCallable, Category = "ALS|HeldObject")
	void AttachToHand(AHoldableObject* NewHoldable, bool bLeftHand, FVector Offset);
	
protected:
	virtual void Tick(float DeltaTime) override;

	virtual void BeginPlay() override;

	virtual void OnOverlayStateChanged(EALSOverlayState PreviousState) override;

	/** Implement on BP to update animation states of held objects */
	UFUNCTION(BlueprintImplementableEvent, BlueprintCallable, Category = "ALS|HeldObject")
	void UpdateHeldObjectAnimations();

public:
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "ALS|Component")
	TObjectPtr<USceneComponent> HeldObjectRoot = nullptr;

	UPROPERTY(Transient, BlueprintReadWrite, Category = "ALS")
	TWeakObjectPtr<AHoldableObject> HeldObject = nullptr;
};
