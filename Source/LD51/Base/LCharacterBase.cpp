﻿// Fill out your copyright notice in the Description page of Project Settings.


#include "LCharacterBase.h"

#include "LD51/Gameplay/HoldableObject.h"

ALCharacterBase::ALCharacterBase(const FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer)
{
	HeldObjectRoot = CreateDefaultSubobject<USceneComponent>(TEXT("HeldObjectRoot"));
	HeldObjectRoot->SetupAttachment(GetMesh());
}

void ALCharacterBase::ClearHeldObject()
{
	if(!HeldObject.IsValid())
	{
		return;
	}
	
	HeldObject->DetachFromActor(FDetachmentTransformRules::KeepWorldTransform);
	HeldObject->OnDropped();
	HeldObject = nullptr;
}

void ALCharacterBase::AttachToHand(AHoldableObject* New, bool bLeftHand, FVector Offset)
{
	ClearHeldObject();

	check(IsValid(New));

	HeldObject = New;

	FName AttachBone;
	if (bLeftHand)
	{
		AttachBone = TEXT("VB LHS_ik_hand_gun");
	}
	else
	{
		AttachBone = TEXT("VB RHS_ik_hand_gun");
	}

	HeldObjectRoot->AttachToComponent(GetMesh(), FAttachmentTransformRules::SnapToTargetNotIncludingScale, AttachBone);
	HeldObjectRoot->SetRelativeLocation(Offset);
	
	HeldObject->OnPickedUp(HeldObjectRoot);
	HeldObject->AttachToComponent(HeldObjectRoot, HeldObject->AutoAttach ? FAttachmentTransformRules::SnapToTargetNotIncludingScale : FAttachmentTransformRules::KeepWorldTransform);
	
}

void ALCharacterBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	UpdateHeldObjectAnimations();
}

void ALCharacterBase::BeginPlay()
{
	Super::BeginPlay();

	UpdateHeldObject();
}


void ALCharacterBase::OnOverlayStateChanged(EALSOverlayState PreviousState)
{
	Super::OnOverlayStateChanged(PreviousState);
	UpdateHeldObject();
}