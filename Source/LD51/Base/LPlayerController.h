﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Character/ALSPlayerController.h"
#include "UObject/Object.h"
#include "LPlayerController.generated.h"

/**
 * 
 */
UCLASS()
class LD51_API ALPlayerController : public AALSPlayerController
{
	GENERATED_BODY()
	
protected:
	UFUNCTION()
	void InteractAction(const FInputActionValue& Value);

};
